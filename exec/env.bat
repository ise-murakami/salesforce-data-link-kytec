@echo off
set APP_BASE=%~dp0
set CLASSPATH=%CLASSPATH%;%APP_BASE%lib\salesforce-data-link-fw.jar
set CLASSPATH=%CLASSPATH%;%APP_BASE%lib\salesforce-data-link-kytec.jar
set CLASSPATH=%CLASSPATH%;%APP_BASE%lib\log4j-1.2.17.jar
set CLASSPATH=%CLASSPATH%;%APP_BASE%lib\ojdbc6.jar
set CLASSPATH=%CLASSPATH%;%APP_BASE%lib\orangesignal-csv-2.2.1.jar
set CLASSPATH=%CLASSPATH%;%APP_BASE%lib\partner.jar
set CLASSPATH=%CLASSPATH%;%APP_BASE%lib\partner.wsdl
set CLASSPATH=%CLASSPATH%;%APP_BASE%lib\wsc-23.jar
set JAVA_HOME=%APP_BASE%jre
set PATH=%JAVA_HOME%\bin
exit /b
