package jp.co.kytec.sfdatalink.csvout;

import jp.co.iseise.sfdatalink.fw.annotation.DateFormat;
import jp.co.iseise.sfdatalink.fw.annotation.FieldDef;
import jp.co.iseise.sfdatalink.fw.annotation.SfObjectType;
import jp.co.iseise.sfdatalink.fw.common.SfBean;

/**
 * @author MURAKAMI
 *
 */
/**
 * @author MURAKAMI
 *
 */
@SfObjectType("Quotation__c")
public class SalesManagementBean implements SfBean {

	@FieldDef(sfField = "", csvSeq = 1)
	private String ad;

	@FieldDef(sfField = "", csvSeq = 2)
	private String eigyoshoCd;

	@FieldDef(sfField = "", csvSeq = 3)
	private String serialNumber;

	@FieldDef(sfField = "", csvSeq = 4)
	private String quoteNoM;

	@FieldDef(sfField = "", csvSeq = 5)
	private String quoteNoS;

	@FieldDef(sfField = "", csvSeq = 6)
	private String gyoshuCd1;

	@FieldDef(sfField = "", csvSeq = 7)
	private String gyoshuName1;

	@FieldDef(sfField = "", csvSeq = 8)
	private String gyoshuCd2;

	@FieldDef(sfField = "", csvSeq = 9)
	private String gyoshuName2;

	@FieldDef(sfField = "", csvSeq = 10)
	private String subjectKana;

	@FieldDef(sfField = "", csvSeq = 11)
	private String subject;

	@FieldDef(sfField = "", csvSeq = 12)
	private String subjectSub;

	@FieldDef(sfField = "", csvSeq = 13)
	private String accountCdM;

	@FieldDef(sfField = "", csvSeq = 14)
	private String accountCdS;

	@FieldDef(sfField = "", csvSeq = 15)
	private String accountName;

	@FieldDef(sfField = "", csvSeq = 16)
	private String designAmount;

	@DateFormat("yyyy/MM")
	@FieldDef(sfField = "", csvSeq = 17)
	private String deliveryDate;

	@FieldDef(sfField = "", csvSeq = 18)
	private String estimatedArea;

	@FieldDef(sfField = "", csvSeq = 19)
	private String planAmountFixed;

	@FieldDef(sfField = "", csvSeq = 20)
	private String planAmount;

	@FieldDef(sfField = "", csvSeq = 21)
	private String deliveryPlaceCd1;

	@FieldDef(sfField = "", csvSeq = 22)
	private String deliveryPlaceCd2;

	@FieldDef(sfField = "", csvSeq = 23)
	private String deliveryPlaceCd3;

	@FieldDef(sfField = "", csvSeq = 24)
	private String deliveryPlace1;

	@FieldDef(sfField = "", csvSeq = 25)
	private String deliveryPlace2;

	@FieldDef(sfField = "", csvSeq = 26)
	private String deliveryPlace3;

	@FieldDef(sfField = "", csvSeq = 27)
	private String salesFormCd;

	@FieldDef(sfField = "", csvSeq = 28)
	private String salesForm;

	@FieldDef(sfField = "", csvSeq = 29)
	private String salesKbnCd;

	@FieldDef(sfField = "", csvSeq = 30)
	private String salesKbn;

	@FieldDef(sfField = "", csvSeq = 31)
	private String tcFlg;

	@FieldDef(sfField = "", csvSeq = 32)
	private String carpetFlg;

	@FieldDef(sfField = "", csvSeq = 33)
	private String carpet1cd;

	@FieldDef(sfField = "", csvSeq = 34)
	private String carpet1;

	@FieldDef(sfField = "", csvSeq = 35)
	private String carpet2cd;

	@FieldDef(sfField = "", csvSeq = 36)
	private String carpet2;

	@FieldDef(sfField = "", csvSeq = 37)
	private String carpetArea;

	@FieldDef(sfField = "", csvSeq = 38)
	private String designCompanyCdM;

	@FieldDef(sfField = "", csvSeq = 39)
	private String designCompanyCdS;

	@FieldDef(sfField = "", csvSeq = 40)
	private String designCompany;

	@FieldDef(sfField = "", csvSeq = 41)
	private String designCompanyPerson;

	@FieldDef(sfField = "", csvSeq = 42)
	private String generalContractorCdM;

	@FieldDef(sfField = "", csvSeq = 43)
	private String generalContractorCdS;

	@FieldDef(sfField = "", csvSeq = 44)
	private String generalContractor;

	@FieldDef(sfField = "", csvSeq = 45)
	private String generalContractorPerson;

	@FieldDef(sfField = "", csvSeq = 46)
	private String designKbnCd;

	@FieldDef(sfField = "", csvSeq = 47)
	private String designKbn;

	@FieldDef(sfField = "", csvSeq = 48)
	private String probabilityCd;

	@FieldDef(sfField = "", csvSeq = 49)
	private String probability;

	@FieldDef(sfField = "", csvSeq = 50)
	private String issuePrice;

	@FieldDef(sfField = "", csvSeq = 51)
	private String groupCd;

	@FieldDef(sfField = "", csvSeq = 52)
	private String groupName;

	@FieldDef(sfField = "", csvSeq = 53)
	private String ownerCd;

	@FieldDef(sfField = "", csvSeq = 54)
	private String owner;

	@DateFormat("yyyyMMdd")
	@FieldDef(sfField = "", csvSeq = 55)
	private String publishedDate;

	@FieldDef(sfField = "", csvSeq = 56)
	private String productKbnCd;

	@FieldDef(sfField = "", csvSeq = 57)
	private String productKbn;

	@FieldDef(sfField = "", csvSeq = 58)
	private String salesItemCd;

	@FieldDef(sfField = "", csvSeq = 59)
	private String salesItem;

	// EG担当者コード
	@FieldDef(sfField = "", csvSeq = 60)
	private String egSalesCd;

	// EG担当
	@FieldDef(sfField = "", csvSeq = 61)
	private String egSales;

	// 販売依頼書No
	@FieldDef(sfField = "", csvSeq = 62)
	private String salesRequestNo;

	// 工事No
	@FieldDef(sfField = "", csvSeq = 63)
	private String constructionNo;

	// 販売指示書発行日
	@FieldDef(sfField = "yyyy/MM/dd", csvSeq = 64)
	private String salesInstructionDate;
	// 備考１
	@FieldDef(sfField = "", csvSeq = 65)
	private String remarks1;
	// 備考２
	@FieldDef(sfField = "", csvSeq = 66)
	private String remarks2;
	// 件名SUB備考１
	@FieldDef(sfField = "", csvSeq = 67)
	private String subjectSubRemarks1;
	// 件名SUB備考２
	@FieldDef(sfField = "", csvSeq = 68)
	private String subjectSubRemarks2;
	// スペック担当者コード
	@FieldDef(sfField = "", csvSeq = 69)
	private String specSalesCd;
	// スペック担当
	@FieldDef(sfField = "", csvSeq = 70)
	private String specSales;

	@FieldDef(sfField = "", csvSeq = 71)
	private String ownerCompany;

	@FieldDef(sfField = "", csvSeq = 72)
	private String amCompany;

	@FieldDef(sfField = "", csvSeq = 73)
	private String pmCompany;

	@FieldDef(sfField = "", csvSeq = 74)
	private String bmCompany;

	/**
	 * @return ad
	 */
	public String getAd() {
		return ad;
	}

	/**
	 * @return eigyoshoCd
	 */
	public String getEigyoshoCd() {
		return eigyoshoCd;
	}

	/**
	 * @return serialNumber
	 */
	public String getSerialNumber() {
		return serialNumber;
	}

	/**
	 * @return quoteNoM
	 */
	public String getQuoteNoM() {
		return quoteNoM;
	}

	/**
	 * @return quoteNoS
	 */
	public String getQuoteNoS() {
		return quoteNoS;
	}

	/**
	 * @return gyoshuCd1
	 */
	public String getGyoshuCd1() {
		return gyoshuCd1;
	}

	/**
	 * @return gyoshuName1
	 */
	public String getGyoshuName1() {
		return gyoshuName1;
	}

	/**
	 * @return gyoshuCd2
	 */
	public String getGyoshuCd2() {
		return gyoshuCd2;
	}

	/**
	 * @return gyoshuName2
	 */
	public String getGyoshuName2() {
		return gyoshuName2;
	}

	/**
	 * @return subjectKana
	 */
	public String getSubjectKana() {
		return subjectKana;
	}

	/**
	 * @return subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @return subjectSub
	 */
	public String getSubjectSub() {
		return subjectSub;
	}

	/**
	 * @return accountCdM
	 */
	public String getAccountCdM() {
		return accountCdM;
	}

	/**
	 * @return accountCdS
	 */
	public String getAccountCdS() {
		return accountCdS;
	}

	/**
	 * @return accountName
	 */
	public String getAccountName() {
		return accountName;
	}

	/**
	 * @return designAmount
	 */
	public String getDesignAmount() {
		return designAmount;
	}

	/**
	 * @return deliveryDate
	 */
	public String getDeliveryDate() {
		return deliveryDate;
	}

	/**
	 * @return estimatedArea
	 */
	public String getEstimatedArea() {
		return estimatedArea;
	}

	/**
	 * @return planAmountFixed
	 */
	public String getPlanAmountFixed() {
		return planAmountFixed;
	}

	/**
	 * @return planAmount
	 */
	public String getPlanAmount() {
		return planAmount;
	}

	/**
	 * @return deliveryPlaceCd1
	 */
	public String getDeliveryPlaceCd1() {
		return deliveryPlaceCd1;
	}

	/**
	 * @return deliveryPlaceCd2
	 */
	public String getDeliveryPlaceCd2() {
		return deliveryPlaceCd2;
	}

	/**
	 * @return deliveryPlaceCd3
	 */
	public String getDeliveryPlaceCd3() {
		return deliveryPlaceCd3;
	}

	/**
	 * @return deliveryPlace1
	 */
	public String getDeliveryPlace1() {
		return deliveryPlace1;
	}

	/**
	 * @return deliveryPlace2
	 */
	public String getDeliveryPlace2() {
		return deliveryPlace2;
	}

	/**
	 * @return deliveryPlace3
	 */
	public String getDeliveryPlace3() {
		return deliveryPlace3;
	}

	/**
	 * @return salesFormCd
	 */
	public String getSalesFormCd() {
		return salesFormCd;
	}

	/**
	 * @return salesForm
	 */
	public String getSalesForm() {
		return salesForm;
	}

	/**
	 * @return salesKbnCd
	 */
	public String getSalesKbnCd() {
		return salesKbnCd;
	}

	/**
	 * @return salesKbn
	 */
	public String getSalesKbn() {
		return salesKbn;
	}

	/**
	 * @return tcFlg
	 */
	public String getTcFlg() {
		return tcFlg;
	}

	/**
	 * @return carpetFlg
	 */
	public String getCarpetFlg() {
		return carpetFlg;
	}

	/**
	 * @return carpet1cd
	 */
	public String getCarpet1cd() {
		return carpet1cd;
	}

	/**
	 * @return carpet1
	 */
	public String getCarpet1() {
		return carpet1;
	}

	/**
	 * @return carpet2cd
	 */
	public String getCarpet2cd() {
		return carpet2cd;
	}

	/**
	 * @return carpet2
	 */
	public String getCarpet2() {
		return carpet2;
	}

	/**
	 * @return carpetArea
	 */
	public String getCarpetArea() {
		return carpetArea;
	}

	/**
	 * @return designCompanyCdM
	 */
	public String getDesignCompanyCdM() {
		return designCompanyCdM;
	}

	/**
	 * @return designCompanyCdS
	 */
	public String getDesignCompanyCdS() {
		return designCompanyCdS;
	}

	/**
	 * @return designCompany
	 */
	public String getDesignCompany() {
		return designCompany;
	}

	/**
	 * @return designCompanyPerson
	 */
	public String getDesignCompanyPerson() {
		return designCompanyPerson;
	}

	/**
	 * @return generalContractorCdM
	 */
	public String getGeneralContractorCdM() {
		return generalContractorCdM;
	}

	/**
	 * @return generalContractorCdS
	 */
	public String getGeneralContractorCdS() {
		return generalContractorCdS;
	}

	/**
	 * @return generalContractor
	 */
	public String getGeneralContractor() {
		return generalContractor;
	}

	/**
	 * @return generalContractorPerson
	 */
	public String getGeneralContractorPerson() {
		return generalContractorPerson;
	}

	/**
	 * @return designKbnCd
	 */
	public String getDesignKbnCd() {
		return designKbnCd;
	}

	/**
	 * @return designKbn
	 */
	public String getDesignKbn() {
		return designKbn;
	}

	/**
	 * @return probabilityCd
	 */
	public String getProbabilityCd() {
		return probabilityCd;
	}

	/**
	 * @return probability
	 */
	public String getProbability() {
		return probability;
	}

	/**
	 * @return issuePrice
	 */
	public String getIssuePrice() {
		return issuePrice;
	}

	/**
	 * @return groupCd
	 */
	public String getGroupCd() {
		return groupCd;
	}

	/**
	 * @return groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @return ownerCd
	 */
	public String getOwnerCd() {
		return ownerCd;
	}

	/**
	 * @return owner
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * @return publishedDate
	 */
	public String getPublishedDate() {
		return publishedDate;
	}

	/**
	 * @return productKbnCd
	 */
	public String getProductKbnCd() {
		return productKbnCd;
	}

	/**
	 * @return productKbn
	 */
	public String getProductKbn() {
		return productKbn;
	}

	/**
	 * @return salesItemCd
	 */
	public String getSalesItemCd() {
		return salesItemCd;
	}

	/**
	 * @return salesItem
	 */
	public String getSalesItem() {
		return salesItem;
	}

	/**
	 * @return remarks1
	 */
	public String getRemarks1() {
		return remarks1;
	}

	/**
	 * @param ad セットする ad
	 */
	public void setAd(String ad) {
		this.ad = ad;
	}

	/**
	 * @param eigyoshoCd セットする eigyoshoCd
	 */
	public void setEigyoshoCd(String eigyoshoCd) {
		this.eigyoshoCd = eigyoshoCd;
	}

	/**
	 * @param serialNumber セットする serialNumber
	 */
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	/**
	 * @param quoteNoM セットする quoteNoM
	 */
	public void setQuoteNoM(String quoteNoM) {
		this.quoteNoM = quoteNoM;
	}

	/**
	 * @param quoteNoS セットする quoteNoS
	 */
	public void setQuoteNoS(String quoteNoS) {
		this.quoteNoS = quoteNoS;
	}

	/**
	 * @param gyoshuCd1 セットする gyoshuCd1
	 */
	public void setGyoshuCd1(String gyoshuCd1) {
		this.gyoshuCd1 = gyoshuCd1;
	}

	/**
	 * @param gyoshuName1 セットする gyoshuName1
	 */
	public void setGyoshuName1(String gyoshuName1) {
		this.gyoshuName1 = gyoshuName1;
	}

	/**
	 * @param gyoshuCd2 セットする gyoshuCd2
	 */
	public void setGyoshuCd2(String gyoshuCd2) {
		this.gyoshuCd2 = gyoshuCd2;
	}

	/**
	 * @param gyoshuName2 セットする gyoshuName2
	 */
	public void setGyoshuName2(String gyoshuName2) {
		this.gyoshuName2 = gyoshuName2;
	}

	/**
	 * @param subjectKana セットする subjectKana
	 */
	public void setSubjectKana(String subjectKana) {
		this.subjectKana = subjectKana;
	}

	/**
	 * @param subject セットする subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @param subjectSub セットする subjectSub
	 */
	public void setSubjectSub(String subjectSub) {
		this.subjectSub = subjectSub;
	}

	/**
	 * @param accountCdM セットする accountCdM
	 */
	public void setAccountCdM(String accountCdM) {
		this.accountCdM = accountCdM;
	}

	/**
	 * @param accountCdS セットする accountCdS
	 */
	public void setAccountCdS(String accountCdS) {
		this.accountCdS = accountCdS;
	}

	/**
	 * @param accountName セットする accountName
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 * @param designAmount セットする designAmount
	 */
	public void setDesignAmount(String designAmount) {
		this.designAmount = designAmount;
	}

	/**
	 * @param deliveryDate セットする deliveryDate
	 */
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	/**
	 * @param estimatedArea セットする estimatedArea
	 */
	public void setEstimatedArea(String estimatedArea) {
		this.estimatedArea = estimatedArea;
	}

	/**
	 * @param planAmountFixed セットする planAmountFixed
	 */
	public void setPlanAmountFixed(String planAmountFixed) {
		this.planAmountFixed = planAmountFixed;
	}

	/**
	 * @param planAmount セットする planAmount
	 */
	public void setPlanAmount(String planAmount) {
		this.planAmount = planAmount;
	}

	/**
	 * @param deliveryPlaceCd1 セットする deliveryPlaceCd1
	 */
	public void setDeliveryPlaceCd1(String deliveryPlaceCd1) {
		this.deliveryPlaceCd1 = deliveryPlaceCd1;
	}

	/**
	 * @param deliveryPlaceCd2 セットする deliveryPlaceCd2
	 */
	public void setDeliveryPlaceCd2(String deliveryPlaceCd2) {
		this.deliveryPlaceCd2 = deliveryPlaceCd2;
	}

	/**
	 * @param deliveryPlaceCd3 セットする deliveryPlaceCd3
	 */
	public void setDeliveryPlaceCd3(String deliveryPlaceCd3) {
		this.deliveryPlaceCd3 = deliveryPlaceCd3;
	}

	/**
	 * @param deliveryPlace1 セットする deliveryPlace1
	 */
	public void setDeliveryPlace1(String deliveryPlace1) {
		this.deliveryPlace1 = deliveryPlace1;
	}

	/**
	 * @param deliveryPlace2 セットする deliveryPlace2
	 */
	public void setDeliveryPlace2(String deliveryPlace2) {
		this.deliveryPlace2 = deliveryPlace2;
	}

	/**
	 * @param deliveryPlace3 セットする deliveryPlace3
	 */
	public void setDeliveryPlace3(String deliveryPlace3) {
		this.deliveryPlace3 = deliveryPlace3;
	}

	/**
	 * @param salesFormCd セットする salesFormCd
	 */
	public void setSalesFormCd(String salesFormCd) {
		this.salesFormCd = salesFormCd;
	}

	/**
	 * @param salesForm セットする salesForm
	 */
	public void setSalesForm(String salesForm) {
		this.salesForm = salesForm;
	}

	/**
	 * @param salesKbnCd セットする salesKbnCd
	 */
	public void setSalesKbnCd(String salesKbnCd) {
		this.salesKbnCd = salesKbnCd;
	}

	/**
	 * @param salesKbn セットする salesKbn
	 */
	public void setSalesKbn(String salesKbn) {
		this.salesKbn = salesKbn;
	}

	/**
	 * @param tcFlg セットする tcFlg
	 */
	public void setTcFlg(String tcFlg) {
		this.tcFlg = tcFlg;
	}

	/**
	 * @param carpetFlg セットする carpetFlg
	 */
	public void setCarpetFlg(String carpetFlg) {
		this.carpetFlg = carpetFlg;
	}

	/**
	 * @param carpet1cd セットする carpet1cd
	 */
	public void setCarpet1cd(String carpet1cd) {
		this.carpet1cd = carpet1cd;
	}

	/**
	 * @param carpet1 セットする carpet1
	 */
	public void setCarpet1(String carpet1) {
		this.carpet1 = carpet1;
	}

	/**
	 * @param carpet2cd セットする carpet2cd
	 */
	public void setCarpet2cd(String carpet2cd) {
		this.carpet2cd = carpet2cd;
	}

	/**
	 * @param carpet2 セットする carpet2
	 */
	public void setCarpet2(String carpet2) {
		this.carpet2 = carpet2;
	}

	/**
	 * @param carpetArea セットする carpetArea
	 */
	public void setCarpetArea(String carpetArea) {
		this.carpetArea = carpetArea;
	}

	/**
	 * @param designCompanyCdM セットする designCompanyCdM
	 */
	public void setDesignCompanyCdM(String designCompanyCdM) {
		this.designCompanyCdM = designCompanyCdM;
	}

	/**
	 * @param designCompanyCdS セットする designCompanyCdS
	 */
	public void setDesignCompanyCdS(String designCompanyCdS) {
		this.designCompanyCdS = designCompanyCdS;
	}

	/**
	 * @param designCompany セットする designCompany
	 */
	public void setDesignCompany(String designCompany) {
		this.designCompany = designCompany;
	}

	/**
	 * @param designCompanyPerson セットする designCompanyPerson
	 */
	public void setDesignCompanyPerson(String designCompanyPerson) {
		this.designCompanyPerson = designCompanyPerson;
	}

	/**
	 * @param generalContractorCdM セットする generalContractorCdM
	 */
	public void setGeneralContractorCdM(String generalContractorCdM) {
		this.generalContractorCdM = generalContractorCdM;
	}

	/**
	 * @param generalContractorCdS セットする generalContractorCdS
	 */
	public void setGeneralContractorCdS(String generalContractorCdS) {
		this.generalContractorCdS = generalContractorCdS;
	}

	/**
	 * @param generalContractor セットする generalContractor
	 */
	public void setGeneralContractor(String generalContractor) {
		this.generalContractor = generalContractor;
	}

	/**
	 * @param generalContractorPerson セットする generalContractorPerson
	 */
	public void setGeneralContractorPerson(String generalContractorPerson) {
		this.generalContractorPerson = generalContractorPerson;
	}

	/**
	 * @param designKbnCd セットする designKbnCd
	 */
	public void setDesignKbnCd(String designKbnCd) {
		this.designKbnCd = designKbnCd;
	}

	/**
	 * @param designKbn セットする designKbn
	 */
	public void setDesignKbn(String designKbn) {
		this.designKbn = designKbn;
	}

	/**
	 * @param probabilityCd セットする probabilityCd
	 */
	public void setProbabilityCd(String probabilityCd) {
		this.probabilityCd = probabilityCd;
	}

	/**
	 * @param probability セットする probability
	 */
	public void setProbability(String probability) {
		this.probability = probability;
	}

	/**
	 * @param issuePrice セットする issuePrice
	 */
	public void setIssuePrice(String issuePrice) {
		this.issuePrice = issuePrice;
	}

	/**
	 * @param groupCd セットする groupCd
	 */
	public void setGroupCd(String groupCd) {
		this.groupCd = groupCd;
	}

	/**
	 * @param groupName セットする groupName
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @param ownerCd セットする ownerCd
	 */
	public void setOwnerCd(String ownerCd) {
		this.ownerCd = ownerCd;
	}

	/**
	 * @param owner セットする owner
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	/**
	 * @param publishedDate セットする publishedDate
	 */
	public void setPublishedDate(String publishedDate) {
		this.publishedDate = publishedDate;
	}

	/**
	 * @param productKbnCd セットする productKbnCd
	 */
	public void setProductKbnCd(String productKbnCd) {
		this.productKbnCd = productKbnCd;
	}

	/**
	 * @param productKbn セットする productKbn
	 */
	public void setProductKbn(String productKbn) {
		this.productKbn = productKbn;
	}

	/**
	 * @param salesItemCd セットする salesItemCd
	 */
	public void setSalesItemCd(String salesItemCd) {
		this.salesItemCd = salesItemCd;
	}

	/**
	 * @param salesItem セットする salesItem
	 */
	public void setSalesItem(String salesItem) {
		this.salesItem = salesItem;
	}

	/**
	 * @param remarks1 セットする remarks1
	 */
	public void setRemarks1(String remarks1) {
		this.remarks1 = remarks1;
	}

	public String getAmCompany() {
		return amCompany;
	}

	public void setAmCompany(String amCompany) {
		this.amCompany = amCompany;
	}

	public String getPmCompany() {
		return pmCompany;
	}

	public void setPmCompany(String pmCompany) {
		this.pmCompany = pmCompany;
	}

	public String getOwnerCompany() {
		return ownerCompany;
	}

	public void setOwnerCompany(String ownerCompany) {
		this.ownerCompany = ownerCompany;
	}

	public String getBmCompany() {
		return bmCompany;
	}

	public void setBmCompany(String bmCompany) {
		this.bmCompany = bmCompany;
	}

	public String getEgSalesCd() {
		return egSalesCd;
	}

	public void setEgSalesCd(String egSalesCd) {
		this.egSalesCd = egSalesCd;
	}

	public String getEgSales() {
		return egSales;
	}

	public void setEgSales(String egSales) {
		this.egSales = egSales;
	}

	public String getSalesRequestNo() {
		return salesRequestNo;
	}

	public void setSalesRequestNo(String salesRequestNo) {
		this.salesRequestNo = salesRequestNo;
	}

	public String getConstructionNo() {
		return constructionNo;
	}

	public void setConstructionNo(String constructionNo) {
		this.constructionNo = constructionNo;
	}

	public String getSalesInstructionDate() {
		return salesInstructionDate;
	}

	public void setSalesInstructionDate(String salesInstructionDate) {
		this.salesInstructionDate = salesInstructionDate;
	}

	public String getSpecSalesCd() {
		return specSalesCd;
	}

	public void setSpecSalesCd(String specSalesCd) {
		this.specSalesCd = specSalesCd;
	}

	public String getSpecSales() {
		return specSales;
	}

	public void setSpecSales(String specSales) {
		this.specSales = specSales;
	}

	public String getRemarks2() {
		return remarks2;
	}

	public void setRemarks2(String remarks2) {
		this.remarks2 = remarks2;
	}

	public String getSubjectSubRemarks1() {
		return subjectSubRemarks1;
	}

	public void setSubjectSubRemarks1(String subjectSubRemarks1) {
		this.subjectSubRemarks1 = subjectSubRemarks1;
	}

	public String getSubjectSubRemarks2() {
		return subjectSubRemarks2;
	}

	public void setSubjectSubRemarks2(String subjectSubRemarks2) {
		this.subjectSubRemarks2 = subjectSubRemarks2;
	}


}
