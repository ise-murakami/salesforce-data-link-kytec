package jp.co.kytec.sfdatalink.apps;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.sforce.soap.partner.sobject.SObject;

import jp.co.iseise.sfdatalink.fw.apps.Sf2Csv;
import jp.co.iseise.sfdatalink.fw.common.Parameter;
import jp.co.iseise.sfdatalink.fw.common.SObjectUtil;
import jp.co.iseise.sfdatalink.fw.common.SfBean;
import jp.co.kytec.sfdatalink.common.Constants;
import jp.co.kytec.sfdatalink.csvout.SalesManagementBean;

public class SalesManagement extends Sf2Csv {
	private static Logger logger = Logger.getLogger(SalesManagement.class);
	// 前回抽出した見積の最も新しいCreatedDateを保存しておく変数
	private String targetCreatedDate;
	// 抽出した見積のうち最も新しいCreatedDateを保存しておく変数
	private String newestCreatedDate;
	// 抽出した見積のIDを保持しておくList
	private List<String> quotationIds = new ArrayList<String>();
	// 比較用
	private static final BigDecimal BD_ONE = new BigDecimal(1);
	// 比較用
	private static final BigDecimal BD_ZERO = new BigDecimal(0);

	/* (非 Javadoc)
	 * @see jp.co.iseise.sfdatalink.fw.apps.Sf2Csv#getCsvHeader()
	 */
	@Override
	protected String getCsvHeader() {
		String[] headerItems = new String[]{
				"西暦年号",
				"営業所コード",
				"連番",
				"見積No-M",
				"見積No-S",
				"業種コード１",
				"業種名１",
				"業種コード２",
				"業種名２",
				"件名フリガナ",
				"件名",
				"件名SUB",
				"得意先CD-M",
				"得意先CD-S",
				"得意先名",
				"設計金額",
				"納期",
				"見積面積",
				"予定金額（固定）",
				"予定金額",
				"納入場所コード１",
				"納入場所コード２",
				"納入場所コード３",
				"納入場所１",
				"納入場所２",
				"納入場所３",
				"販売形態コード",
				"販売形態",
				"販売区分コード",
				"販売区分",
				"TCの有無フラグ",
				"カーペットの有無",
				"カーペット品番１コード",
				"カーペット品番１",
				"カーペット品番２コード",
				"カーペット品番２",
				"カーペット面積",
				"設計会社CD-M",
				"設計会社CD-S",
				"設計会社",
				"設計会社（担当者）",
				"ゼネコンCD-M",
				"ゼネコンCD-S",
				"ゼネコン",
				"ゼネコン（担当者）",
				"建築区分コード",
				"建築区分",
				"受注確度コード",
				"受注確度",
				"号価",
				"グループコード",
				"グループ名",
				"担当者コード",
				"営業担当",
				"見積発行日",
				"製品区分コード",
				"製品区分",
				"販売品目コード",
				"販売品目",
				"ＥＧ担当者コード",
				"ＥＧ担当",
				"販売依頼書No",
				"工事No",
				"販売指示書発行日",
				"備考１",
				"備考２",
				"件名SUB備考１",
				"件名SUB備考２",
				"スペック担当者コード",
				"スペック担当",
				"施主",
				"AM会社",
				"PM会社",
				"BM会社"
		};
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < headerItems.length; i++) {
			if (i != 0) {
				sb.append(param.get(Parameter.OUTPUT_CSV_SEPARATOR));
			}
			sb.append(param.get(Parameter.OUTPUT_CSV_QUOTE_CHAR));
			sb.append(headerItems[i]);
			sb.append(param.get(Parameter.OUTPUT_CSV_QUOTE_CHAR));
		}
		return sb.toString();
	}

	/* (非 Javadoc)
	 * @see jp.co.iseise.sfdatalink.fw.apps.Sf2Csv#init()
	 */
	@Override
	protected boolean init() {
		targetCreatedDate = super.manageFile.get(Constants.EXTRACTED);
		// 抽出した見積の最新CreatedDateはデフォルトで前回のCreatedDateにしておく
		newestCreatedDate = targetCreatedDate;
		return true;
	}


	/* (非 Javadoc)
	 * @see jp.co.iseise.sfdatalink.fw.apps.Sf2Csv#getInputSoql()
	 */
	@Override
	protected String getInputSoql() {
		String soql = "SELECT"
				+ " Id"														// 見積ID
				+ ",Opportunity__r.Quotation__c"							// 物件情報の見積ID
				+ ",PublishedDate__c"										// 発行日
				+ ",Opportunity__r.Office__r.OfficeCd__c"					// 営業所コード
				+ ",QuotationNo__c"											// 見積No.
				+ ",QuotationBranchNo__c"									// 見積No.（枝番）
				+ ",Opportunity__r.TypesOfIndustry1__c"						// 業種1
				+ ",toLabel(Opportunity__r.TypesOfIndustry1__c) TypesOfIndustry1__c_Label"	// 業種1（トランスレーションワークベンチ）
				+ ",Opportunity__r.TypesOfIndustry2__c"						// 業種2
				+ ",toLabel(Opportunity__r.TypesOfIndustry2__c) TypesOfIndustry2__c_Label"	// 業種2（トランスレーションワークベンチ）
				+ ",ArticleKana__c"											// 件名カナ
				+ ",Name"													// 件名
				+ ",Name2__c"												// 件名SUB
				+ ",Account__r.Name"										// 得意先名
				+ ",TotalQuotationPrice__c"									// 総見積額
				+ ",UsedDesignPrice__c"										// 使用設計
				+ ",Opportunity__r.CloseDate"								// 納期
				+ ",NwM2__c"												// NW㎡
				+ ",PlanPrice__c"											// 予定金額
				+ ",DeliveryLocationCD__c"									// 納入場所CD
				+ ",DeliveryLocation1__c"									// 納入場所1
				+ ",DeliveryLocation2__c"									// 納入場所2
				+ ",DeliveryLocation3__c"									// 納入場所3
				+ ",Form__c"												// 形態
				+ ",Opportunity__r.DesignCompanyLookUp__r.Name"				// 設計会社
				+ ",Opportunity__r.GeneralContractorLookUp__r.Name"			// ゼネコン
				+ ",Opportunity__r.build_rebuildingreconstruction__c"		// 建築区分
				+ ",toLabel(Opportunity__r.build_rebuildingreconstruction__c) build_rebuildingreconstruction__c_Label"	// 建築区分（トランスレーションワークベンチ）
//				+ ",Opportunity__r.StageName"								// 受注確度
				+ ",Opportunity__r.Owner.EmployeeNumber"					// 従業員番号
				+ ",Opportunity__r.Owner.Name"								// 名前
				+ ",ProductDiv__c"											// 製品区分
				+ ",toLabel(ProductDiv__c) ProductDiv__c_Label"				// 製品区分（トランスレーションワークベンチ）
				+ ",SaleItemDiv__c"											// 販売品目
				+ ",toLabel(SaleItemDiv__c) SaleItemDiv__c_Label"			// 販売品目（トランスレーションワークベンチ）
				+ ",CreatedDate"											// 作成日付
				+ ",TcM2__c"												// TC㎡
				+ ",Group__c"												// グループ
				+ ",toLabel(Group__c) Group__c_Label"						// グループ（トランスレーションワークベンチ）
				+ ",Opportunity__r.AMCompanyLookUp__r.Name"					// 施主
				+ ",Opportunity__r.AMCompany__r.Name"						// AM会社
				+ ",Opportunity__r.PMCompanyLookUp__r.Name"					// PM会社
				+ ",Opportunity__r.BMCompany__r.Name"						// BM会社
				+ ",Ashidaka__c"											// 脚高
				+ ",BillingNo__c"											// 販売・請求番号
				+ ",ConstructionNo__c"										// 工事番号
				+ ",BillingNoPublishedDate__c"								// 販売・請求番号発行日
				+ ",Opportunity__r.SpecSales__r.Name"						// スペック担当者
				+ ",Opportunity__r.SpecSales__r.EmployeeNumber__c"			// スペック担当者従業員番号
				+ ",Opportunity__r.CsSales__r.Name"							// CS担当者
				+ ",Opportunity__r.CsSales__r.EmployeeNumber__c"			// CS担当者従業員番号
				+ ",ReportCheckRemarks__c"									// 帳票チェック用備考
				+ ",AssistantRemarks1__c"									// アシスタントメモ用備考１
				+ ",AssistantRemarks2__c"									// アシスタントメモ用備考２
				+ ",Opportunity__r.AccuracyOfOrder__c"						// 受注確度
				+ ",Welfare__c"												// 法定福利費適用
				+ ",WelfareCost__c"											// 法定福利費
				+ ",WelfareFixedCost__c"									// 法定福利費（固定）
				+ ",ConstructionFrom__c"									// 工期（From）
				+ " FROM Quotation__c" //
				+ " WHERE IsSyncing__c = true"								// 有効のみ
		;

		// パラメータの見積No
		String targetQuotationNo = super.commonProperties.get("TARGET_QUOTATION_NO");
		if (targetQuotationNo == null || targetQuotationNo.length() == 0) {
			// 見積Noが指定されていない場合
//			// 未抽出のレコードのみ対象
//			soql += " AND Extracted__c = false";
//			if (targetCreatedDate == null || targetCreatedDate.length() == 0) {
//				// 前回の抽出履歴が無い場合は全件対象のため、ここでは条件を追加しない
//			} else {
//				// 前回の抽出履歴がある場合はその日時より新しいCreatedDateの見積を抽出する
//				soql += " AND CreatedDate > " + targetCreatedDate;
//			}
		} else {
			// 見積Noが指定されている場合
			// 見積Noはカンマ区切りで複数指定するため、カンマで分割してIN句のパラメータを作成する
			String[] nos = targetQuotationNo.split(",");
			StringBuilder p = new StringBuilder();
			p.append("(");
			for (int i = 0; i < nos.length; i++) {
				if (i != 0) {
					p.append(",");
				}
				p.append("'" + nos[i] + "'");
			}
			p.append(")");
			soql += " AND QuotationNo__c IN " + p.toString();
		}
		// パラメータの営業所コード
		String salesOfficeCd = super.commonProperties.get("SALES_OFFICE_CD");
		if (salesOfficeCd != null && salesOfficeCd.length() != 0) {
			// 営業所コードが指定されている場合
			soql += " AND Opportunity__r.Office__r.OfficeCd__c = '" + salesOfficeCd + "'";
		}
		soql += " ORDER BY CreatedDate LIMIT " + param.get(Parameter.SF_SHORI_KENSU_PER_TRAN);
		return soql;
	}

	/* (非 Javadoc)
	 * @see jp.co.iseise.sfdatalink.fw.apps.Sf2Csv#setupSfBean()
	 */
	@Override
	protected SfBean setupSfBean() {
		return new SalesManagementBean();
	}

	/* (非 Javadoc)
	 * @see jp.co.iseise.sfdatalink.fw.apps.Sf2Csv#editCsv(com.sforce.soap.partner.sobject.SObject, java.util.Map, jp.co.iseise.sfdatalink.fw.common.SfBean, int)
	 */
	@Override
	protected void editCsv(SObject record, Map<String, String> fieldInfoMap, SfBean sfBean, int i) {
		SalesManagementBean csvBean = (SalesManagementBean) sfBean;
		// ID
		quotationIds.add(SObjectUtil.getString(record, "Id"));
		// 西暦年号
		if (SObjectUtil.getString(record, "PublishedDate__c").length() != 0) {
			csvBean.setAd(SObjectUtil.getStringAsDate(record, "PublishedDate__c", "yy"));
		}
		// 営業所コード
		csvBean.setEigyoshoCd(SObjectUtil.getString(record, "Opportunity__r.Office__r.OfficeCd__c"));
		// 連番
		// 見積No-M
		csvBean.setQuoteNoM(SObjectUtil.getString(record, "QuotationNo__c"));
		// 見積No-S
		if (SObjectUtil.getString(record, "QuotationBranchNo__c").length() != 0) {
			csvBean.setQuoteNoS(String.format("%02d", SObjectUtil.getInt(record, "QuotationBranchNo__c")));
		}
		// 業種コード1
		csvBean.setGyoshuCd1(SObjectUtil.getString(record, "Opportunity__r.TypesOfIndustry1__c"));
		// 業種名1
		String tmp = SObjectUtil.getString(record, "Opportunity__r.TypesOfIndustry1__c_Label");
		csvBean.setGyoshuName1(tmp.equals("-") ? "" : tmp);
		// 業種コード2
		csvBean.setGyoshuCd2(SObjectUtil.getString(record, "Opportunity__r.TypesOfIndustry2__c"));
		// 業種名2
		tmp = SObjectUtil.getString(record, "Opportunity__r.TypesOfIndustry2__c_Label");
		csvBean.setGyoshuName2(tmp.equals("-") ? "" : tmp);
		// 件名フリガナ
		csvBean.setSubjectKana(SObjectUtil.getString(record, "ArticleKana__c"));
		// 件名
		csvBean.setSubject(SObjectUtil.getString(record, "Name"));
		// 件名SUB
		csvBean.setSubjectSub(SObjectUtil.getString(record, "Name2__c"));
		// 得意先CD-M
		// 得意先CD-S
		// 得意先名
		csvBean.setAccountName(SObjectUtil.getString(record, "Account__r.Name"));
		// 設計金額
		csvBean.setDesignAmount(SObjectUtil.getBigDecimal(record, "UsedDesignPrice__c").setScale(0).toPlainString());
		// 納期
//		csvBean.setDeliveryDate(SObjectUtil.getStringAsDate(record, "Opportunity__r.CloseDate", "yyyy/MM"));
		tmp = SObjectUtil.getString(record, "ConstructionFrom__c");
		if (tmp.length() >= 7) {
			csvBean.setDeliveryDate(tmp.substring(0,  7));
		} else {
			csvBean.setDeliveryDate(tmp);
		}
		// 見積面積
		csvBean.setEstimatedArea(SObjectUtil.getBigDecimal(record, "NwM2__c").setScale(0, BigDecimal.ROUND_HALF_UP).toPlainString());
		// 予定金額（固定）
		// 予定金額
//		csvBean.setPlanAmount(SObjectUtil.getBigDecimal(record, "PlanPrice__c").setScale(0).toPlainString());
        long welfareCost;
        String welfare = SObjectUtil.getString(record, "Welfare__c");
        if ("する【掛け率】".equals(welfare)) {
            welfareCost = SObjectUtil.getLong(record, "WelfareCost__c");
        }
        else if ("する【固定】".equals(welfare)) {
            welfareCost = SObjectUtil.getLong(record, "WelfareFixedCost__c");
        }
        else{
            welfareCost = 0;
        }
        // 予定金額
        csvBean.setPlanAmount(String.valueOf(SObjectUtil.getLong(record, "PlanPrice__c") + welfareCost));

		// 納入場所コード１
		tmp = SObjectUtil.getString(record, "DeliveryLocationCD__c");
		if (tmp.length() >= 2) {
			csvBean.setDeliveryPlaceCd1(tmp.substring(0,  2));
		}
		// 納入場所コード２
		if (tmp.length() >= 4) {
			csvBean.setDeliveryPlaceCd2(tmp.substring(2,  4));
		}
		// 納入場所コード３
		if (tmp.length() >= 6) {
			csvBean.setDeliveryPlaceCd3(tmp.substring(4,  6));
		}
		// 納入場所１
		csvBean.setDeliveryPlace1(SObjectUtil.getString(record, "DeliveryLocation1__c"));
		// 納入場所２
		csvBean.setDeliveryPlace2(SObjectUtil.getString(record, "DeliveryLocation2__c"));
		// 納入場所３
		csvBean.setDeliveryPlace3(SObjectUtil.getString(record, "DeliveryLocation3__c"));
		// 販売形態コード
		tmp = SObjectUtil.getString(record, "Form__c");
		csvBean.setSalesFormCd(tmp.indexOf("材販") != -1 ? "1" : "2");
		// 販売形態
		csvBean.setSalesForm(tmp.indexOf("材販") != -1 ? "材販" : "材工");
		// 販売区分コード
		// 販売区分
		BigDecimal carpetArea = SObjectUtil.getBigDecimal(record, "TcM2__c").setScale(0, BigDecimal.ROUND_HALF_UP);
		if (carpetArea.compareTo(BD_ONE) == 1 || carpetArea.compareTo(BD_ONE) == 0) {
			// カーペット面積が1以上の場合
			// TCの有無フラグ
			csvBean.setTcFlg("2");
			// カーペットの有無
			csvBean.setCarpetFlg("有");
			// カーペット面積
			csvBean.setCarpetArea(carpetArea.toPlainString());
		} else if (carpetArea.compareTo(BD_ZERO) == -1 || carpetArea.compareTo(BD_ZERO) == 0) {
			// カーペット面積が0以下の場合
			// TCの有無フラグ
			csvBean.setTcFlg("1");
			// カーペットの有無
			csvBean.setCarpetFlg("無");
		}
		// カーペット品番１コード
		// カーペット品番１
		// カーペット品番２コード
		// カーペット品番２
		// 設計会社CD-M
		// 設計会社CD-S
		// 設計会社
		csvBean.setDesignCompany(SObjectUtil.getString(record, "Opportunity__r.DesignCompanyLookUp__r.Name"));
		// 設計会社（担当者）
		// ゼネコンCD-M
		// ゼネコンCD-S
		// ゼネコン
		csvBean.setGeneralContractor(SObjectUtil.getString(record, "Opportunity__r.GeneralContractorLookUp__r.Name"));
		// ゼネコン（担当者）
		// 建築区分コード
		csvBean.setDesignKbnCd(SObjectUtil.getString(record, "Opportunity__r.build_rebuildingreconstruction__c"));
		// 建築区分
		csvBean.setDesignKbn(SObjectUtil.getString(record, "Opportunity__r.build_rebuildingreconstruction__c_Label"));
		// 受注確度を一旦取得
		String s = SObjectUtil.getString(record, "Opportunity__r.AccuracyOfOrder__c");
		if (s != null && s.length() >= 2) {
			// 受注確度コード
			switch (s.substring(0, 2)) {
			case "未定":
				csvBean.setProbabilityCd("1");
				break;
			case "有力":
				csvBean.setProbabilityCd("2");
				break;
			case "内定":
				csvBean.setProbabilityCd("3");
				break;
			case "決定":
				csvBean.setProbabilityCd("4");
				break;
			case "失注":
				csvBean.setProbabilityCd("5");
				break;
			default:
				csvBean.setProbabilityCd("");
			}
			// 受注確度
			csvBean.setProbability(s.substring(0, 2));
		} else {
			// 受注確度コード
			csvBean.setProbabilityCd("");
			// 受注確度
			csvBean.setProbability("");
		}
		// 号価
		// グループコード
		csvBean.setGroupCd(SObjectUtil.getString(record, "Group__c"));
		// グループ名
		csvBean.setGroupName(SObjectUtil.getString(record, "Group__c_Label"));
		// 担当者コード
		csvBean.setOwnerCd(SObjectUtil.getString(record, "Opportunity__r.Owner.EmployeeNumber"));
		// 営業担当
		csvBean.setOwner(SObjectUtil.getString(record, "Opportunity__r.Owner.Name"));
		// 見積発行日
		if (SObjectUtil.getString(record, "PublishedDate__c").length() != 0) {
			csvBean.setPublishedDate(SObjectUtil.getStringAsDate(record, "PublishedDate__c", "yyyy/MM/dd"));
		}
		// 製品区分コード
		csvBean.setProductKbnCd(SObjectUtil.getString(record, "ProductDiv__c").replaceAll("P", ""));
		// 製品区分
		csvBean.setProductKbn(SObjectUtil.getString(record, "ProductDiv__c_Label"));
		// 販売品目コード
		csvBean.setSalesItemCd(SObjectUtil.getString(record, "SaleItemDiv__c"));
		// 販売品目
		csvBean.setSalesItem(SObjectUtil.getString(record, "SaleItemDiv__c_Label"));
		// ＥＧ担当者コード
		csvBean.setEgSalesCd(SObjectUtil.getString(record, "Opportunity__r.CsSales__r.EmployeeNumber__c"));
		// ＥＧ担当
		csvBean.setEgSales(SObjectUtil.getString(record, "Opportunity__r.CsSales__r.Name"));
		// 販売依頼書No
		String temp = SObjectUtil.getString(record, "BillingNo__c");
		temp = temp == null || temp.length() == 0 ? "" : temp.substring(0, 6);
		csvBean.setSalesRequestNo(temp);
		// 工事No
		csvBean.setConstructionNo(SObjectUtil.getString(record, "ConstructionNo__c"));
		// 販売指示書発行日※nullとするため、ここでは設定しない
//		if (SObjectUtil.getString(record, "BillingNoPublishedDate__c").length() != 0) {
//			csvBean.setSalesInstructionDate(SObjectUtil.getStringAsDate(record, "BillingNoPublishedDate__c", "yyyy/MM/dd"));
//		}
		// 備考１
		csvBean.setRemarks1(SObjectUtil.getString(record, "Ashidaka__c"));
		// 備考２
		csvBean.setRemarks2(SObjectUtil.getString(record, "ReportCheckRemarks__c"));
		// 件名SUB備考１
		csvBean.setSubjectSubRemarks1(SObjectUtil.getString(record, "AssistantRemarks1__c"));
		// 件名SUB備考２
		csvBean.setSubjectSubRemarks2(SObjectUtil.getString(record, "AssistantRemarks2__c"));
		// スペック担当者コード
		csvBean.setSpecSalesCd(SObjectUtil.getString(record, "Opportunity__r.SpecSales__r.EmployeeNumber__c"));
		// スペック担当
		csvBean.setSpecSales(SObjectUtil.getString(record, "Opportunity__r.SpecSales__r.Name"));
		// 施主
		csvBean.setOwnerCompany(SObjectUtil.getString(record, "Opportunity__r.AMCompanyLookUp__r.Name"));
		// AM会社
		csvBean.setAmCompany(SObjectUtil.getString(record, "Opportunity__r.AMCompany__r.Name"));
		// PM会社
		csvBean.setPmCompany(SObjectUtil.getString(record, "Opportunity__r.PMCompanyLookUp__r.Name"));
		// BM会社
		csvBean.setBmCompany(SObjectUtil.getString(record, "Opportunity__r.BMCompany__r.Name"));
		// 作成日（次回取り込みの日付の基準とする）
		// ORDER BY CreatedDateとしているので、当変数には最終的に最も新しい見積の作成日付が入る
		newestCreatedDate = SObjectUtil.getString(record, "CreatedDate");

	}
	/* (非 Javadoc)
	 * @see jp.co.iseise.sfdatalink.fw.apps.Sf2Csv#end()
	 */
	@Override
	protected boolean end() {
		//出力中にエラーが発生した場合、ファイルを削除
		if (errFlg) {
			File delFile = new File(filePath);
			delFile.delete();
			return false;
		}

		// パラメータの見積No
		String targetQuotationNo = super.commonProperties.get("TARGET_QUOTATION_NO");
		if (targetQuotationNo == null || targetQuotationNo.length() == 0) {
			// パラメータの見積Noが無い場合のみ抽出した見積の最新CreatedDateをプロパティファイルに保存しておく
			super.manageFile.set(Constants.EXTRACTED, newestCreatedDate);
		}
		return true;
//		// 見積.抽出済を更新する
//		SaveResult[] updateResults;
//		try {
//			List<SObject> soList = new ArrayList<SObject>();
//			for (String quotationId : quotationIds) {
//				// 更新対象のSObjectを作成
//				SObject so = new SObject();
//				so.setType("Quotation__c");
//				so.setId(quotationId);
//				so.setField("Extracted__c", true);
//				soList.add(so);
//			}
//			updateResults = partnerConnection.update(soList.toArray(new SObject[0]));
//			boolean error=false;
//			for (int i = 0; i <  updateResults.length; i++) {
//				SaveResult ur = updateResults[i];
//				if (ur.getSuccess()) {
//					logger.info("見積.抽出済チェックボックスの更新に成功しました。Id=" + ur.getId());
//				} else {
//					com.sforce.soap.partner.Error[] errors = ur.getErrors();
//					for (com.sforce.soap.partner.Error er : errors) {
//						logger.error(er.getMessage() + " Id=" + soList.get(i).getId());
//					}
//					error = true;
//				}
//			}
//			if(error) {
//				logger.error("見積.抽出済チェックボックスの更新に失敗しました。");
//				return false;
//			}
//			// 正常終了
//			return true;
//
//		} catch (ConnectionException e) {
//			logger.error("見積.抽出済チェックボックスの更新に失敗しました。", e);
//			return false;
//		}
	}
}
